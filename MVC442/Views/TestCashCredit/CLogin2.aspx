﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CLogin2.aspx.cs" Inherits="Module_CLogin2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ล็อคอินผ่านระบบ OTP</title>

    <script src="../scripts/jquery-1.9.1.js"></script> 
    <script src="../scripts/jquery-ui-1.10.3.custom.js"></script>
    <script src="../scripts/DateMask.js"></script>   
    <script src="../scripts/validateForm.js"></script>  
    <script src="../scripts/jquery.maskedinput.js"></script>
    <link href="../scripts/datepicker/css/redmond/jquery-ui-1.10.3.custom.css" rel="stylesheet" />
	
 
         <!--///// CSS Custom ///// -->  
        <link href="~/css/reset.css" rel="stylesheet" />
        <link href="~/css/fontface.css" rel="stylesheet" />
        <link href="~/css/layoutBasic.css" rel="stylesheet" />   
		
		
	   <!--///// Bootstrap UI  ///// --> 
	    <link href="../bootstrap/bootstrap.css" rel="stylesheet" />   
        <link href="../bootstrap/bootstrap-responsive.css" rel="stylesheet" />
		 <script src="../bootstrap/bootstrap.js"></script>
			
    <script>
        function validateSMSForm() {
            var isRet = true;
            var strMsg = "";
            var strFocus = "";
            if (document.getElementById("<%=txtTelephoneNo.ClientID %>").value == "") {
                isRet = false;
                strMsg += "    - หมายเลขโทรศัพท์\n";
                if (strFocus == "")
                    strFocus = document.getElementById("<%=txtTelephoneNo.ClientID %>");
            }
            if (isRet == false) {
                alert("! กรุณาระบุข้อมูล : \n" + strMsg);
                if (strFocus != "")
                    eval(strFocus.focus());
            }
            return isRet;
        }
        function validateForm() {
            var isRet = true;
            var strMsg = "";
            var strFocus = "";
            if (document.getElementById("<%=txtTelephoneNo.ClientID %>").value == "") {
                isRet = false;
                strMsg += "    - หมายเลขโทรศัพท์\n";
                if (strFocus == "")
                    strFocus = document.getElementById("<%=txtTelephoneNo.ClientID %>");
            }
            if (document.getElementById("<%=txtOTP.ClientID %>").value == "") {
                isRet = false;
                strMsg += "    - รหัสผ่าน\n";
                if (strFocus == "")
                    strFocus = document.getElementById("<%=txtOTP.ClientID %>");
            }
            if (isRet == false) {
                alert("! กรุณาระบุข้อมูล : \n" + strMsg);
                if (strFocus != "")
                    eval(strFocus.focus());
            }
            return isRet;
        }
    </script>
</head>
<body style="background:none; "  >

 
    <form id="form1" runat="server">
    <div id="  page-online-insCar-Detail   "   class="row" >
   
   <div class="col-sm-12">
<div  class="text-center">
     <img src="../../CashCredit/images/Logo_OPT.jpg"  class=" img-responsive"/> 
 </div>      

<h4 class="col-md-12"  >
    1. กรุณาระบุหมายเลขโทรศัพท์   เพื่อรับรหัสผ่านแบบใช้ครั้งเดียว
</h4>
 
<div class="form-group">
    <label class="control-label col-md-2" for="Message">หมายเลขโทรศัพท์ :</label>
 
    <div class="col-md-10">
        <asp:TextBox ID="txtTelephoneNo" runat="server"  CssClass="form-control text-box single-line" ></asp:TextBox> 
   
    </div>
</div><!-- /.form-group --> 

<div class="form-group"> 
    <div class="col-md-10">
        <asp:Button ID="btnSMS" runat="server" Text="รับรหัสผ่าน" onclick="btnSMS_Click" OnClientClick="return validateSMSForm();"  class="btn btn-default"  />
    </div>
</div><!-- /.form-group --> 
  
<div class="clearfix" ></div> 

<h4  class="col-md-12"  >
 2. กรุณาใส่รหัสผ่านที่ได้รับทาง SMS ในช่องว่างด้านล่าง
    และกดตกลงภายใน 15 นาที
</h4>

<div class="form-group">
    <label class="control-label col-md-2" for="Message">รหัสผ่าน :</label> 

    <div class="col-md-10"> 
        <asp:TextBox ID="txtOTP" runat="server"  CssClass="form-control text-box single-line" ></asp:TextBox>  

    </div>
</div><!-- /.form-group -->

<div class="form-group"> 
    <div class="col-md-10">
        <asp:Button ID="btnSave" runat="server" Text="ตกลง"
                    onclick="btnSave_Click" OnClientClick="return validateForm();" CssClass="btn btn-default" Style="margin-right: 10px;" />
        <asp:Button ID="btnCancel" runat="server" Text="ยกเลิก" OnClientClick="window.location = 'CLogin.aspx'" CssClass=" btn  btn-default" />
    </div>
</div><!-- /.form-group --> 




 
 
 
 
<div class="form-group"   style="display:none"   > 
    <label class="control-label col-md-2 subject2" for="Message">หมายเหตุ : </label> 
	
    <div class="col-md-10">
  <asp:TextBox ID="txtRemark" runat="server"  CssClass="form-control text-box single-line"  ></asp:TextBox>
    </div>
</div><!-- /.form-group --> 


<div class="form-group"   style="display:none"   > 
    <label class="control-label col-md-2 subject2" for="Message">SMS Status :</label> 
	
    <div class="col-md-10">
   <asp:TextBox ID="txtSMSStatus" runat="server"   CssClass="form-control text-box single-line"  ></asp:TextBox>
    </div>
	 <asp:Literal ID="litJavaScript" runat="server"></asp:Literal>    
</div><!-- /.form-group --> 


   </div> <!-- /.col-sm-12 -->
 </div> <!-- /.row -->
                       
        </div>
    </form>
</body>
</html>
