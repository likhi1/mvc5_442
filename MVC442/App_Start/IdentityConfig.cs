﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using MVC442.Models;
//==========================
using System.Configuration;
using System.Net.Mail; // sendMail
using System.Net; // sendMail


namespace MVC442
{
   
 
    // Configure the application user manager used in this application. UserManager is defined in ASP.NET Identity and is used by the application.
    public class ApplicationUserManager : UserManager<ApplicationUser, int> {
        public ApplicationUserManager(IUserStore<ApplicationUser, int> store)  : base(store)    {
        } 

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context) 
        {
            var manager = new ApplicationUserManager(
                new UserStoreIntPk (context.Get<ApplicationDbContext>())
                );
            // Configure validation logic for usernames
            manager.UserValidator = new UserValidator<ApplicationUser, int>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true
            };

            // Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
               // RequireNonLetterOrDigit = true,
               // RequireDigit = true,
              //  RequireLowercase = true,
               // RequireUppercase = true,
            };

            // Configure user lockout defaults
            manager.UserLockoutEnabledByDefault = true;
            manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
            manager.MaxFailedAccessAttemptsBeforeLockout = 5;

            // Register two factor authentication providers. This application uses Phone and Emails as a step of receiving a code for verifying the user
            // You can write your own provider and plug it in here.
            manager.RegisterTwoFactorProvider("Phone Code", new PhoneNumberTokenProvider<ApplicationUser, int> {
                MessageFormat = "Your security code is {0}"
            });
            manager.RegisterTwoFactorProvider("Email Code", new EmailTokenProvider<ApplicationUser, int> {
                Subject = "Security Code",
                BodyFormat = "Your security code is {0}"
            });
            manager.EmailService = new EmailService();
            manager.SmsService = new SmsService();
            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider = 
                    new DataProtectorTokenProvider<ApplicationUser, int>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
            return manager;
        }
    }


    //Likhit  12/17/2015  Increse    ApplicationRoleManager  For  Role Management 
    // PASS CUSTOM APPLICATION ROLE AND INT AS TYPE ARGUMENTS TO BASE:
    public class ApplicationRoleManager : RoleManager<ApplicationRole, int> {
        // PASS CUSTOM APPLICATION ROLE AND INT AS TYPE ARGUMENTS TO CONSTRUCTOR:
        public ApplicationRoleManager(IRoleStore<ApplicationRole, int> roleStore)
            : base(roleStore) {
        }

        // PASS CUSTOM APPLICATION ROLE AS TYPE ARGUMENT:
        public static ApplicationRoleManager Create(
            IdentityFactoryOptions<ApplicationRoleManager> options, IOwinContext context) {
            return new ApplicationRoleManager( 
                new ApplicationRoleStore(context.Get<ApplicationDbContext>()));
        }
    }




    // Configure the application sign-in manager which is used in this application.
    public class ApplicationSignInManager : SignInManager<ApplicationUser, int>
    {
        public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
            : base(userManager, authenticationManager)
        {
        }

        public override Task<ClaimsIdentity> CreateUserIdentityAsync(ApplicationUser user)
        {
            return user.GenerateUserIdentityAsync((ApplicationUserManager)UserManager);
        }

        public static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
        {
            return new ApplicationSignInManager(context.GetUserManager<ApplicationUserManager>(), context.Authentication);
        }
    }
 


    public class EmailService : IIdentityMessageService
    {

        //\\\\\\\\\\\\\\\\\\\\  ASyncrinize Method
        public Task SendAsync(IdentityMessage IdM)
        {
            // Credentials:
            var SMTPUsername = ConfigurationManager.AppSettings["SMTPUsername"];
            var SMTPPassword = ConfigurationManager.AppSettings["SMTPPassword"];
            var SendTitleEmail = ConfigurationManager.AppSettings["SendTitleEmail"];
            var SendTitleString = ConfigurationManager.AppSettings["SendTitleString"];

            // Configure the SmtpClient:
            System.Net.Mail.SmtpClient smtpClient = new System.Net.Mail.SmtpClient(); // Use Inline NameSpace
            smtpClient.Host = ConfigurationManager.AppSettings["SMTPHost"];
            smtpClient.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"]);
            smtpClient.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            smtpClient.UseDefaultCredentials = false;

            // Creatte the credentials:
            System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(SMTPUsername, SMTPPassword);

            smtpClient.EnableSsl = true;
            smtpClient.Credentials = credentials;

            // Create the message:
            var mailMessage = new System.Net.Mail.MailMessage();
            mailMessage.From = new MailAddress(SendTitleEmail, SendTitleString, System.Text.Encoding.UTF8); // Set Send Mail  
            mailMessage.To.Add(IdM.Destination);
            mailMessage.Subject = IdM.Subject;
            mailMessage.Body = IdM.Body;

            // mailMessage.CC.Add(CC); //  Set this send to user 
            // mailMessage.Bcc.Add(ForwardEmail1);   
            // mailMessage.Bcc.Add(ForwardEmail2);    

            // Send:
            return smtpClient.SendMailAsync(mailMessage);      // Send by System.Net.Mail
        }


        //\\\\\\\\\\\\\\\\\\\\  Syncrinize Method
        public void SendSmtpSyncrinize(IdentityMessage IdM)
        {
            var SMTPUsername = ConfigurationManager.AppSettings["SMTPUsername"];
            var SMTPPassword = ConfigurationManager.AppSettings["SMTPPassword"];
            var SendTitleEmail = ConfigurationManager.AppSettings["SendTitleEmail"];
            var SendTitleString = ConfigurationManager.AppSettings["SendTitleString"];

            // Configure the SmtpClient:
            SmtpClient smtpClient = new SmtpClient();
            smtpClient.Host = ConfigurationManager.AppSettings["SMTPHost"];
            smtpClient.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"]);
            smtpClient.EnableSsl = true;
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpClient.UseDefaultCredentials = false;

            // Creatte the credentials:
            smtpClient.Credentials = new NetworkCredential(SMTPUsername, SMTPPassword);

            // Create the message:
            using (var mailMessage = new MailMessage())
            {  // MailUsername, ToEmail 
                mailMessage.From = new MailAddress(SendTitleEmail, SendTitleString, System.Text.Encoding.UTF8); // Set Send Mail  
                mailMessage.To.Add(IdM.Destination);
                mailMessage.Subject = IdM.Subject;
                mailMessage.Body = IdM.Body;
                mailMessage.IsBodyHtml = true;

                // mailMessage.CC.Add(CC); //  Set this send to user 
                // mailMessage.Bcc.Add(ForwardEmail1);    
                // mailMessage.Bcc.Add(ForwardEmail2);    

                // Send:
                smtpClient.Send(mailMessage);
            }



        }
         

    }

    public class SmsService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            // Plug in your SMS service here to send a text message.
            return Task.FromResult(0);
        }
    }


}// end nameSpace
