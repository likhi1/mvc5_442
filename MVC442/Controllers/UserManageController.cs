﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC442.Models;
//================= 
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Net;

namespace MVC442.Controllers
{
    [Authorize]
    public class UserManageController : Controller
    {

        ApplicationUserManager  _userManager;
        public ApplicationUserManager UserManager {
            get {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set {
                _userManager = value;
            }
        }

        ApplicationRoleManager _roleManager;
        
        public ApplicationRoleManager RoleManager {
            get {
                return _roleManager;
            }
            set {
                _roleManager = value;
            }
        }

        private MVC442Entities db = new MVC442Entities();

        // GET: UserManage
        public ActionResult Index()
        {
           var model =  db.AspNetUsers.ToList();

            return View(model);
        }
 

        public async  Task<ActionResult>  Edit (  int id ) { 
            if(id> 0 ){
                var user = await UserManager.FindByIdAsync(id); 
                if(user == null) {
                    return HttpNotFound();
                }

                var userRoles = await UserManager.GetRolesAsync(user.Id);
                return View(new AspNetUsersViewModel() {
                    Id = user.Id,
                    Email = user.Email ,
                    NameDisplay =  user.NameDisplay,
                    FirstName = user.FirstName ,
                    LastName =  user.LastName ,
                    PhoneNumber =  user.PhoneNumber 
                }); 

           } 
            return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }




        //
        // POST: /Users/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Email,Id,NameDisplay")] AspNetUsersViewModel editUser, params string[] selectedRole) {
            if (ModelState.IsValid) {
                var user = await UserManager.FindByIdAsync(editUser.Id);
                if (user == null) {
                    return HttpNotFound();
                }

                user.UserName = editUser.Email;
                user.Email = editUser.Email;
                user.NameDisplay = editUser.NameDisplay;

                var userRoles = await UserManager.GetRolesAsync(user.Id);

                selectedRole = selectedRole ?? new string[] { };

                var result = await UserManager.AddToRolesAsync(user.Id, selectedRole.Except(userRoles).ToArray<string>());

                if (!result.Succeeded) {
                    ModelState.AddModelError("", result.Errors.First());
                    return View();
                }
                result = await UserManager.RemoveFromRolesAsync(user.Id, userRoles.Except(selectedRole).ToArray<string>());

                if (!result.Succeeded) {
                    ModelState.AddModelError("", result.Errors.First());
                    return View();
                }
                return RedirectToAction("Index", "UserManage");
            }
            ModelState.AddModelError("", "Something failed.");
            return View();
        }


        protected override void Dispose(bool disposing) {
            if (disposing) {
                db.Dispose();
            }
            base.Dispose(disposing);
        }



    }
}