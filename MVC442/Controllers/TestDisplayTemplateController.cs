﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//===============  
using MVC442.Models;
using MVC442.Models.ViewModel;
using System.Net;
 

namespace MVC442.Controllers
{
    public class TestDisplayTemplateController : Controller
    {
        private MVC442Entities db = new MVC442Entities();


        public ActionResult Index(int? page) { 

           //=========== Get Data to show
           var model = db.SlideManages 
                .OrderByDescending(x => x.SlideId)
                .ThenBy(x => x.SlideSort)  

                .Select(x => new SlideManageVM {
                    SlideId = x.SlideId
                    , SlideName = x.SlideName
                    , SlideSort = x.SlideSort
                    , SlideStatus = x.SlideStatus
                    , SlideDateShow = x.SlideDateShow
                    , SlideDateHide = x.SlideDateHide
                    , SlideInputDate = x.SlideInputDate
                });
 
            return View(model );
        }
    }
}