﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//==========   
using MVC442.Models;
using MVC442.Models.ViewModel;
using System.Net.Mail; // Send mail
using System.Diagnostics; // For  Debug.WriteLine(ex.Message)  ///// Debug to output panel 
using MVC442.Helper.BL;  // referance method in  folder Helper
using MVC442.Helper.UT;  // referance method in  folder Helper
using reCAPTCHA.MVC;
using System.Net.Http;
using System.Threading.Tasks;

namespace MVC442.Controllers
{
    public class HomeController : BaseController
    {
        private MVC442Entities db = new MVC442Entities();

        ///////////  Called before the action method is invoked
        ///  https://www.google.com/search?sourceid=chrome-psyapi2&ion=1&espv=&ie=UTF-8&q=OnActionExecuting%20get%20data
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
        }


        #region MasterPage

        //============================= Use  @Html.Action  For   Retrive   Data   
        [ChildActionOnly]  // protect retive action direct 
        public ActionResult LiveScoreLinkPartial(int contype)
        {
            var model = db.WebLinkLiveScores
            .Where(x => x.ConType == contype)
            .OrderByDescending(x => x.IdCon)
            .Select(x => new WebLinkLiveScoreVM { IdCon = x.IdCon, WebLink = x.WebLink, WebName = x.WebName })
            .ToList();

            return PartialView("FontEnd/_LiveScoreLinkPartial", model);  //  This  Use PartialView For Show Data
        }


        [ChildActionOnly]  // protect retive action direct 
        public ActionResult SlideManagePartialQuery()
        {

            var model = db.SlideManages
            .Where(x => x.SlideStatus == 1
                && (DateTime.Now >= x.SlideDateShow && DateTime.Now <= x.SlideDateHide)
            )
            .OrderBy(x => x.SlideSort)
            .ThenBy(x => x.SlideDateHide)
            .ThenBy(x => x.SlideId)
            .Select(x => new SlideManageVM { SlideName = x.SlideName, SlideDes = x.SlideDes })
            .ToList();

            return PartialView("FontEnd/_SlideManagePartialQuery", model); //  This  Use PartialView For Show Data 

        }

        public ActionResult SlideManageBootstrapPartial()
        {
            return PartialView("FontEnd/_SlideManagePartial"); //  This  Use PartialView For Show Data  
        }

        [ChildActionOnly]  // protect retive action direct 
        public ActionResult BankLinkPatial(bool typeDevice)
        {   // true = Mobile , false = noneMobile

            object model;
            string partialSelect;

            model = db.BankWebLinks
                 .Where(x => x.BankStaus == 1)
                 .OrderBy(x => x.BankSort).ThenBy(x => x.IdCon)
                 .Select(x => new BankWebLinkVM
                 {
                     IdCon = x.IdCon,
                     BankLink = x.BankLink,
                     BankName = x.BankName,
                     IconPathImage = x.IconPathImage,
                     IconPathFlash = x.IconPathFlash
                 })
                 .ToList();

            if (typeDevice == true) { 
                partialSelect = "FontEnd/_BankLinkHtmlPatial";
            } else { 
                partialSelect = "FontEnd/_BankLinkHtmlPatial";
                //partialSelect = "FontEnd/_BankLinkFlashPatial";
            }

            return PartialView(partialSelect, model); //  This  Use PartialView For Show Data 
        }

        [ChildActionOnly]  // protect retive action direct 
        public ActionResult SlotLogoPartialDynamic(int type, bool mobile = true)
        { // type1 = Sport ,  type2 = Casino
            object model;
            string view;

            if (type == 1)
            {
                model = db.CasinoCats.OrderBy(x => x.IdCasinoCat)
                  .Where(x => x.IdCasinoCat <= 3);
            }
            else
            {
                model = db.CasinoCats.OrderBy(x => x.IdCasinoCat)
                  .Where(x => x.IdCasinoCat > 3 && x.IdCasinoCat <= 11);
            }

            ViewBag.type = type;

            if (mobile == true)
                view = "FontEnd/_SlotLogoHtml5Partial";
            else
                view = "FontEnd/_SlotLogoFlashPartial";

            return PartialView(view, model);
        }

        [ChildActionOnly]  // protect retive action direct 
        public ActionResult SlotLogoPartialStatic(int type, bool mobile = true) { // type1 = Sport ,  type2 = Casino 
            string view;  
            ViewBag.type = type; 
                view = "FontEnd/_SlotLogoHtml5StaticPartial"; 
            return PartialView(view);
        }


        #endregion MasterPage




        #region StaticPage
        //===================================  All StaticPage  
        public ActionResult index()
        {
            return RedirectToAction("home");
        }
        public ActionResult Home2()
        {

            return View();
        }

        public ActionResult Home()
        {

            return View();
        }

        public ActionResult Promotion()
        {
            return View();
        }


        public ActionResult _Slot()
        {

            var model = db.CasinoCats.OrderByDescending(x => x.IdCasinoCat);
            return View(model);
        }



        public ActionResult GetDataSlide()
        {
            return RedirectToAction("home");
        }

        public ActionResult About()
        {
            return View();
        }

        public ActionResult Join()
        {
            return View();
        }

        public ActionResult Deposit()
        {
            return View();
        }

        public ActionResult Download()
        {
            return View();
        }

         
        public ActionResult WorldCupAsia()
        {
            return View();
        }

        public ActionResult Matchfootball()
        {
            return View();
        }
        public ActionResult Livefootball() {
            return View();
        }

        public ActionResult Scorefootball()
        {
            return View();
        }
   
        public ActionResult TableScorefootball() {
            return View();
        }
        public async Task<ActionResult> Scoreleage2() {
            string page = "http://football.kapook.com/%E0%B8%9E%E0%B8%A3%E0%B8%B5%E0%B9%80%E0%B8%A1%E0%B8%B5%E0%B8%A2%E0%B8%A3%E0%B9%8C%E0%B8%A5%E0%B8%B5%E0%B8%81/table";
            HttpClient client = new HttpClient();
            string response = await client.GetStringAsync(page);  // await convert Task<string> to string

            return View();
        }

        public ActionResult Scoreleage3() {
            return View();
        }


        /// <summary>
        /// Test run Async 
        /// </summary>
        /// <returns></returns>
        public static async Task<string> AccessTheWebAsync() {
            string page = "http://football.kapook.com/%E0%B8%9E%E0%B8%A3%E0%B8%B5%E0%B9%80%E0%B8%A1%E0%B8%B5%E0%B8%A2%E0%B8%A3%E0%B9%8C%E0%B8%A5%E0%B8%B5%E0%B8%81/table";

            ////// Ex1
            HttpClient client = new HttpClient(); // Use HttpClient.
            Task<string> getStringTask = client.GetStringAsync(page);  //  Task GetStringAsync( ) Start
            DoIndipendence(); //  run 
            string urlContents = await getStringTask;// await convert Task<string> to string 
            // Any work  can  except use varible urlContents  have to wait
            return urlContents;

            ///// Ex2
            //var response = await client.GetAsync(page);
            //HttpContent content = response.Content;
            //string result = await content.ReadAsStringAsync();
            //return result; 
        }
        public static void DoIndipendence() {

        }

        public ActionResult Onlinefootball()
        {
            return View();
        }

        public ActionResult DownloadProgram1()
        {
            return View();
        }
        public ActionResult DownloadProgram2()
        {
            return View();
        }

        public ActionResult test()
        {
            var model = db.SlideManages
            .Where(x => x.SlideStatus == 1
                && (DateTime.Now >= x.SlideDateShow && DateTime.Now <= x.SlideDateHide)
            )
            .OrderBy(x => x.SlideSort)
            .ThenBy(x => x.SlideId)
            .Select(x => new SlideManageVM { SlideName = x.SlideName, SlideDes = x.SlideDes })
            .ToList();

            //return PartialView("_SlideManagePartial", model); //  This  Use PartialView For Show Data

            return View(model);

        }


 



        #endregion StaticPage



        #region DinamicPage
         //===================================  All DinamicPage   
         [HttpGet]
        public ActionResult OpenAccount(int id = 0)
        {
            var VM = new ClientRegisterVM
            {   // Set Model by ViewModel    
                  SelectListItems = new CategoryManage().GetCasinoCats()   //\\\\\\\  Get DropDownListFor Member (SelectListItems)  by type  from BL    
            };

            ViewBag.CloseDialog = false;
             return View("OpenAccount", VM); //\\  Get retrun  View &  Data   for  iframe   URL
            // return PartialView("FontEnd/_OpenAccountPartial", VM);   //\\  Get retrun  PartialView &  Data  for  iframe  URL
        }


        [HttpPost]   
        [CaptchaValidator(
        PrivateKey = "6LdXUA4TAAAAABEmkI72n069uRFPjTc0P_UPspKL",
        ErrorMessage = "Invalid input captcha.",
        RequiredMessage = "The captcha field is required.")]
        public ActionResult OpenAccount(ClientRegisterVM VM  ,   bool  captchaValid)
        {
            if (captchaValid == true)
            {
                TempData["Message"] = "Captcha ถูกต้อง.";
                TempData["ErrorMessage"] = "";
            }
            else
            {
                TempData["Message"] = "";
                TempData["ErrorMessage"] = "ระบุ Captcha ไม่ถูกต้อง ";
            }


            //\\\\\\\\\\\\\  Check  IsValid 
            if (ModelState.IsValid)
            {
                var strSms = (VM.GetSms == true) ? "รับSMS" : "ไม่รับSMS";
                var strNews = (VM.GetNews == true) ? "รับข่าวสาร" : "ไม่รับข่าวสาร";

                ClientRegister model = new ClientRegister
                {
                    IdCasinoCat = VM.IdCasinoCatSelected,  //\\\\\\\\\\\\\\  Get  Selected Value  by  BaseVM.cs  
                    Name = VM.Name,
                    Tel1 = VM.Tel1,
                    Email = VM.Email, // Email's Client 
                    GetSms = VM.GetSms,
                    GetNews = VM.GetNews,
                    InputDate = DateTime.Now,
                    Ip = Request.UserHostAddress
                };

                //\\\\\\\\\\\\\\ Insert to Database
                db.ClientRegisters.Add(model);
                db.SaveChanges();

                #region SendMailProcss

                #region SendMailContent
                /////// Start sET  E-Mail
                string contentMail = "<h3>ข้อมูลการขอสมัครเปิดบัญชีของลูกค้า - คุณ " + model.Name + "</h3>"
                                + "<table  border='0' cellspacing='1' cellpadding='8' style='font-size:12px ; background-color:#CCCCCC' ; width: 100% ; >"
                                + "<tr>"
                                + "<td colspan='2' style='font-weight:bold; color:#000 ; background-color:#ddd; font-size:13px'> 442.im - Open Account </td>"
                                + "</tr>"

                                + "<tr>"
                                + "<td width='20%' bgcolor='#FFFFFF' style='font-weight:bold; text-align:right ;  color:#666'> ประเภทบริการ : </td>"
                                + "<td width='70%' bgcolor='#FFFFFF'>" + new CategoryManage().GetCatName(model.IdCasinoCat) + "</td>"
                                + "</tr>"
                                //  new CategoryManage().GetCasinoCats();

                                + "<tr>"
                                + "<td width='20%' bgcolor='#FFFFFF' style='font-weight:bold; text-align:right ;  color:#666'> Date : </td>"
                                + "<td width='70%' bgcolor='#FFFFFF'>" + Utillity.SetLocalTimeZone(model.InputDate) + "</td>"
                                + "</tr>"

                                + "<tr>"
                                + "<td width='20%' bgcolor='#FFFFFF' style='font-weight:bold; text-align:right ;  color:#666'> Name : </td>"
                                + "<td width='70%' bgcolor='#FFFFFF'>" + model.Name + "</td>"
                                + "</tr>"

                                + "<tr>"
                                + "<td width='20%' bgcolor='#FFFFFF' style='font-weight:bold; text-align:right ;  color:#666'> Email : </td>"
                                + "<td width='70%' bgcolor='#FFFFFF'>" + model.Email + "</td>"
                                + "</tr>"

                                + "<tr>"
                                + "<td width='20%' bgcolor='#FFFFFF' style='font-weight:bold; text-align:right ;  color:#666'> Tel : </td>"
                                + "<td width='70%' bgcolor='#FFFFFF'>" + model.Tel1 + "</td>"
                                + "</tr>"

                                + "<tr>"
                                + "<td width='20%' bgcolor='#FFFFFF' style='font-weight:bold; text-align:right ;  color:#666'> Get SMS : </td>"
                                + "<td width='70%' bgcolor='#FFFFFF'>" + strSms + "</td>"
                                + "</tr>"

                                + "<tr>"
                                + "<td width='20%' bgcolor='#FFFFFF' style='font-weight:bold; text-align:right ;  color:#666'> Get SMS : </td>"
                                + "<td width='70%' bgcolor='#FFFFFF'>" + strNews + "</td>"
                                + "</tr>"

                                + "<tr>"
                                + "<td width='20%' bgcolor='#FFFFFF' style='font-weight:bold; text-align:right ;  color:#666'> IP : </td>"
                                + "<td width='70%' bgcolor='#FFFFFF'>" + model.Ip + "</td>"
                                + "</tr>"

                                + "</table>";
                #endregion

                //\\\\\\\\\\\\\  Send E-Mail   
                try
                { // Set Option Mail 
                    SmtpMailer smtpMailer = new SmtpMailer();
                    smtpMailer.CC = model.Email;  // Email Client 
                    smtpMailer.Subject = "442.im Open Account" + model.Name;
                    smtpMailer.IsHtml = true;
                    smtpMailer.Body = contentMail;
                    smtpMailer.Send();
                    ViewBag.IsSendMail = true;
                }
                catch (Exception ex)
                {
                    //Debug.WriteLine(ex.Message); 
                    ViewBag.IsSendMail = false;
                }
                #endregion

                ViewBag.FinishProcess = true; //  Correct Insert & Close Jquery Dialog    

                ModelState.Clear(); // Clearing All Model State 
                var NewVM = new ClientRegisterVM
                {   // !!!! Set Empty ViewModel  & Set DropDownListFor Member  
                    SelectListItems = new CategoryManage().GetCasinoCats()   //\\\\\\\  Get DropDownListFor Member (SelectListItems)  by type  from BL    
                };
                return View("OpenAccount", NewVM);
                // return View();   //\\\\\\ return new EmptyResult(); // Like use  void  

            } // end  if (ModelState.IsValid)  


            //\\\\\\\\\\\\\\\\ Not Valid- Return ViewModel   
            ViewBag.FinishProcess = false;   // Not Correct Insert & Not Close Jquery Dialog  
            VM.SelectListItems = new CategoryManage().GetCasinoCats();  //\\\\\\\  Get DropDownListFor Member (SelectListItems)  by type  from BL  
            return View("OpenAccount", VM);
        }

        public ActionResult CasinoOnline(int type = 0, int idCat = 0)
        {
            var model = db.WebLinkCasinoes
             .Where(x => x.ConType == type && x.IdCasinoCat == idCat)
             //.OrderBy(x => x.Sort == null )//\\\\\\ Sort by   ASC  x.Sort and Show null on Bottom
             .OrderBy(x => x.Sort)
             .ThenBy(x => x.IdCon)
             .Select(x => new WebLinkCasinoVM
             {
                 IdCon = x.IdCon,
                 WebLink = x.WebLink,
                 WebName = x.WebName,
                 Description = x.Description,
                 No = x.No
             })
             .ToList();

            //var model = (from c in db.WebLinkCasinoes
            //             orderby c.Sort, c.IdCon descending
            //             where (c.ConType == type && c.IdCasinoCat == idCat) select c).ToList();

            ViewData["idCat"] = idCat;
            return View(model);
        }

        public ActionResult SportOnline(int type = 0, int idCat = 0)
        {
            //var model = from c in db.WebLinkCasinoes orderby c.IdCon where (c.ConType == type && c.IdCasinoCat == idCat) select c;
            var model = db.WebLinkCasinoes
            .Where(x => x.ConType == type && x.IdCasinoCat == idCat)
               //.OrderBy(x => x.Sort == null )//\\\\\\ Sort by   ASC  x.Sort and Show null on Bottom
               .OrderBy(x => x.Sort)
             .ThenBy(x => x.IdCon)
            .Select(x => new WebLinkCasinoVM
            {
                IdCon = x.IdCon,
                WebLink = x.WebLink,
                WebName = x.WebName,
                Description = x.Description,
                No = x.No
            })
            .ToList();

            ViewData["idCat"] = idCat;
            return View(model);
        }

        [HttpGet]
        public ActionResult Contact(int idCat = 1)
        {
            ClientMessageVM VM = new ClientMessageVM
            {
                IdCat = idCat  /// SetId for this  clientMessageVM1
            };
            return View("Contact", VM);// Send viewmodel into UI (View)
        }

        [HttpPost]
        [CaptchaValidator(
        PrivateKey = "6LdXUA4TAAAAABEmkI72n069uRFPjTc0P_UPspKL",
        ErrorMessage = "Invalid input captcha.",
        RequiredMessage = "The captcha field is required.")]
        public ActionResult Contact(ClientMessageVM VM , bool captchaValid )
        { 
            if(captchaValid == true)  {
                TempData["Message"] = "Captcha ถูกต้อง.";
                TempData["ErrorMessage"] = "";
            }  else {
                TempData["Message"] = "";
                 TempData["ErrorMessage"] = "ระบุ Captcha ไม่ถูกต้อง ";
            }

            //\\\\\\\\\\\\\  Check  IsValid 
            if (ModelState.IsValid) {
                //TempData["Message"] = "captcha is valid."; // Set Captcha Response Message 

                //========  Set Model from ViewModel 
                ClientMessage model = new ClientMessage  {
                    IdCat = VM.IdCat,
                    Name = VM.Name,
                    Message = VM.Message,
                    Tel1 = VM.Tel1,
                    Email = VM.Email,  // Email's Client 
                    InputDate = DateTime.Now,//////// Change To Server DateTime /////
                    Ip = Request.UserHostAddress
                };


                //\\\\\\\\\\\\\\ Insert to Database
                db.ClientMessages.Add(model);
                db.SaveChanges();

                ModelState.Clear(); //clearing model  
                ViewBag.FinishProcess = true; //  Correct Insert & Close Jquery Dialog    
                #region SendMail
                #region SenmailContent
                if (ViewBag.Result == true)
                {
                    //======== Set Email Template
                    string contentMail = "<h3>ข้อมูลการส่งข้อความใหม่จากลูกค้า - คุณ " + model.Name + "</h3>"
                        + "<table  border='0' cellspacing='1' cellpadding='8' style='font-size:12px ; background-color:#CCCCCC' ; width: 100% ; >"
                         + "<tr>"
                         + "<td colspan='2' style='font-weight:bold; color:#000 ; background-color:#ddd; font-size:13px'> 442.im - Client Message </td>"
                         + "</tr>"
                         + "<tr>"
                         + "<td width='20%' bgcolor='#FFFFFF' style='font-weight:bold; text-align:right ;  color:#666'> Date : </td>"
                         + "<td width='70%' bgcolor='#FFFFFF'>" + Utillity.SetLocalTimeZone(model.InputDate) + "</td>"
                         + "</tr>"

                         + "<tr>"
                         + "<td width='20%' bgcolor='#FFFFFF' style='font-weight:bold; text-align:right ;  color:#666'> Name : </td>"
                         + "<td width='70%' bgcolor='#FFFFFF'>" + model.Name + "</td>"
                         + "</tr>"

                         + "<tr>"
                         + "<td width='20%' bgcolor='#FFFFFF' style='font-weight:bold; text-align:right ;  color:#666'> Email : </td>"
                         + "<td width='70%' bgcolor='#FFFFFF'>" + model.Email + "</td>"
                         + "</tr>"

                         + "<tr>"
                         + "<td width='20%' bgcolor='#FFFFFF' style='font-weight:bold; text-align:right ;  color:#666'> Tel : </td>"
                         + "<td width='70%' bgcolor='#FFFFFF'>" + model.Tel1 + "</td>"
                         + "</tr>"

                         + "<tr>"
                         + "<td width='20%' bgcolor='#FFFFFF' style='font-weight:bold; text-align:right ;  color:#666'> Message : </td>"
                         + "<td width='70%' bgcolor='#FFFFFF'>" + model.Message + "</td>"
                         + "</tr>"

                         + "<tr>"
                         + "<td width='20%' bgcolor='#FFFFFF' style='font-weight:bold; text-align:right ;  color:#666'> IP : </td>"
                         + "<td width='70%' bgcolor='#FFFFFF'>" + model.Ip + "</td>"
                         + "</tr>"

                         + "</table>";
                    #endregion
                    //\\\\\\\\\\\\\  Send E-Mail   
                    try
                    { // Set Option Mail 
                        SmtpMailer smtpMailer = new SmtpMailer();
                        smtpMailer.CC = model.Email;  // Email Client 
                        smtpMailer.Subject = "442.im Client Message" + model.Name;
                        smtpMailer.IsHtml = true;
                        smtpMailer.Body = contentMail;
                        smtpMailer.Send();
                        ViewBag.IsSendMail = true;
                    }
                    catch (Exception ex)
                    {
                        //Debug.WriteLine(ex.Message); 
                        ViewBag.IsSendMail = false;
                    }

                    #endregion

                    return View();
                } // end if (ModelState.IsValid)  

            }

            //////// Not Valid & Not Captcha  
            ViewBag.FinishProcess = false; //  Check Insert  Finish 
            return View(VM);
        }

        public ActionResult Callcenter(int Idcat = 2)
        {
            var clientMessageVM = new ClientMessageVM(); // Initialize ViewModel
            clientMessageVM.IdCat = Idcat; //  SetId for this  clientMessageVM2
            return View("CallCenter", clientMessageVM); // Send viewmodel into UI (View)
        }

        [HttpPost]
        public ActionResult Callcenter(ClientMessage model)
        {
            model.InputDate = DateTime.Now;
            model.Ip = Request.UserHostAddress;
            if (ModelState.IsValid)
            {
                db.ClientMessages.Add(model);
                db.SaveChanges();
                return RedirectToAction("List");
            }
            return View(model);
        }


        [HttpGet]
        public ActionResult RadioChanel(int ch = 1)
        {
            string viewName = "radio" + ch;
            return View(viewName);
        }

        #endregion DinamicPage



    }
}