﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//===============
using System.Web.Mvc;
using PagedList;
using System.Net;   // set page List 
using MVC442.Models;
using MVC442.Models.ViewModel;
using MVC442.Helper.UT;  // referance method in  folder Helper 
using System.Globalization;  // DateTime Region

namespace MVC442.Controllers
{
    public class TestDateTimePickerBootstap3MalotController : BaseController {
        private MVC442Entities db = new MVC442Entities();

        public ActionResult Index(int? page) {
            //============ Set Status - Some Slide  OverDateHide
      

            //=========== Get Data to show
            var model = db.SlideManages
                .OrderByDescending(x => x.SlideId)
                .ThenBy(x => x.SlideSort) 
                .Select(x => new SlideManageVM {
                    SlideId = x.SlideId 
                    , SlideDateShow = x.SlideDateShow
                    , SlideDateHide = x.SlideDateHide
                    , SlideInputDate = x.SlideInputDate
                });

            //========== Add Page PagedList 
            if (Request.HttpMethod != "GET") {
                page = 1;
            }
            int pageSize = 50;
            int pageNumber = (page ?? 1);

            return View(model.ToPagedList(pageNumber, pageSize));
        }



        public ActionResult Create() {
            SlideManageVM viewModel = new SlideManageVM();
            return View(viewModel);
        }


        public ActionResult Edit(int id = 0) {

            SlideManage slidemanage = db.SlideManages.Find(id);
            if (slidemanage == null) {
                return HttpNotFound();
            }
             
            SlideManageVM viewModel = new SlideManageVM {
                SlideId = slidemanage.SlideId,
                SlideName = slidemanage.SlideName,
                SlideLink = slidemanage.SlideLink,
                SlideDes = slidemanage.SlideDes,
                SlideStatus = slidemanage.SlideStatus,
                SlideSort = slidemanage.SlideSort != 9999 ? slidemanage.SlideSort : null, //  If default value not show data

                //////// Change To Local DateTime /////
                SlideDateShow = Utillity.SetLocalTimeZone(slidemanage.SlideDateShow),
                SlideDateHide = Utillity.SetLocalTimeZone(slidemanage.SlideDateHide)
            };

            return View(viewModel);
        }

    

}
}