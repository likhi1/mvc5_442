﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//===============  
using MVC442.Models;
using MVC442.Models.ViewModel;
using System.Net;



namespace MVC442.Controllers
{
    public class TestEditorTemplateController : Controller
    {
        private MVC442Entities db = new MVC442Entities();


        public ActionResult Index()
        { 
            var model = db.SlideManages.Take(1)
            .OrderByDescending(x => x.SlideId) 
            .Select(x => new SlideManageVM {
                SlideId = x.SlideId 
                , SlideDateShow = x.SlideDateShow
                , SlideDateHide = x.SlideDateHide
                , SlideInputDate = x.SlideInputDate
            });

       


            return View(model );
        }

         

        public ActionResult  Create()
        {
            return View();
        }

        public ActionResult   Edit(int? id = 5)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            SlideManage model = db.SlideManages.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }

            var VM = new SlideManageVM
            {
                SlideId = model.SlideId,
                SlideDes = model.SlideDes,
                SlideInputDate = model.SlideInputDate,
                SlideDateShow = model.SlideDateShow,
                SlideDateHide = model.SlideDateHide

            };

            return View(VM);
        }


        [HttpPost]
        public ActionResult Edit(SlideManageVM VM)
        {

            return View(VM);
        }



 
        public ActionResult TemplateModel(int? id = 5)
        
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            SlideManage model = db.SlideManages.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }

            var VM = new SlideManageVM
            {
                SlideId = model.SlideId,
                SlideDes = model.SlideDes,
                SlideInputDate = model.SlideInputDate,
                SlideDateShow = model.SlideDateShow,
                SlideDateHide = model.SlideDateHide

            };

            return View(VM);
        }




    }
}