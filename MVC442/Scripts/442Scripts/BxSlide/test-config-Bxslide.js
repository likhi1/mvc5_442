
//////========================  Global Var ===================/////
//////// BxSlide & Youtube Varible
var areaShowVideo;
var videoId;
var bxslider1ViewportSize;
//var ytPlayer;  
var objSlider; //  Control BxSlide  start & stop
var currentSlide ;

  
//////===================  BxSlide  Basic =========================/////
function hideBxSlider(_elmId1) {
    _elmId1.parent().hide();
}


function bxSliderBasic(_selectorSlide, _pager, _pause) {
    _selectorSlide.bxSlider({
        mode: 'fade',  //  'horizontal', 'vertical', 'fade'
        auto: true,
        controls: false,
        pager: _pager,
        pause: _pause,
        autoHover: true, 
        pagerType: 'full',
        //startingSlide: 0 
    });
}
 
 
$(function () {
    //========== Run BxSlide1  With YouTube  (Banner Top ) 
    //bxSliderYoutube($("#bxslider1"), true, true, 3000); //5500  
   // bxSliderBasic($("#bxslider1"), true, true, 3000); //5500  

    //========== Run BxSlide2  ( Banner SideBar )
    bxSliderBasic($("#bxslider2"), false, 200); //4500 

    //========== Run BxSlide3 ( Banner Float )
    bxSliderBasic($("#bxslider3"), false, 4500); //4500 
    
}); // End Document Ready

 
