﻿!function ($) {
	$(function () {

		$('.control-datetime').datetimepicker({
			format: 'L LT',
			sideBySide: true,
		});

		$('.control-date').datetimepicker({
			format: 'L',
		});

		$('.control-time').datetimepicker({
			format: 'HH:mm',
		});

	});
}(window.jQuery);
