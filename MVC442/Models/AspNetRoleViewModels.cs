using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//============
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using MVC442.Models;
using MVC442.Helper.UT;  // referance method in  folder Helper
//==================
using System.Globalization;
namespace MVC442.Models
{ 
    public class AspNetRoleViewModels  {

        
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
 
}
