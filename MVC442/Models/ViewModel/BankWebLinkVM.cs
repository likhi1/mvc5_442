﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC442.Models.ViewModel {
    public class BankWebLinkVM {
        [Display(Name = "NO.")]
        public int IdCon { get; set; }

        [Display(Name = "ชื่อธนาคาร")]
        [Required(ErrorMessage = "กรุณาระบุ-ชื่อธนาคาร")]
        public string BankName { get; set; }

        [Display(Name = "ลิงค์ธนาคาร")]
        [Required(ErrorMessage = "กรุณาระบุ-ลิงค์ธนาคาร")]
        [DataType(DataType.MultilineText)] 
        public string BankLink  { get; set; }

        [Display(Name = "ลำดับ")]
        public Nullable<int> BankSort { get; set; }

        [Display(Name = "สถานะ")]
        public Nullable<int> BankStaus { get; set; }

        [Display(Name = "วัน-เวลา")]
        public Nullable<System.DateTime> InputDate { get; set; }


        [Required(ErrorMessage = "กรุณาระบุ-Path Image")]
        [Display(Name = "Path Image")]
        public string IconPathImage { get; set; }

        [Required(ErrorMessage = "กรุณาระบุ-Path Flash")]
        [Display(Name = "Path Flash")]
        public string IconPathFlash { get; set; }


        public string BankLinkTrim {
            get {
                if ((BankLink.Length) > 20)
                    return BankLink.Substring(0, 20) + "...";
                else
                    return BankLink;
            }

        }


        //================= Extended  ViewModel
        string  IconImage;
        [Display(Name = "ไอคอน Image")]
        [Required(ErrorMessage = "กรุณาระบุ-ไอคอน Image")]
        public string IconPathImageTrimed {
            get {
                IconImage = IconPathImage.Replace("/Upload/images/bankIcon/", "");
                //IconImageName =  IconImages.Split('.')[0] ;
                return IconImage;
            } 
        }


        string IconFlash;
        [Display(Name = "ไอคอน Flash")]
        [Required(ErrorMessage = "กรุณาระบุ-ไอคอน Flash")]
        public string IconPathFlashTrimed {
            get {
                IconFlash = IconPathFlash.Replace("/Upload/flash/", "");
                //IconFlashName =  IconFlash.Split('.')[0] ;
                return IconFlash;
            }
        }

    }
}