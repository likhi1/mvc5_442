﻿using System;
using System.Collections.Generic;  
using System.Linq;
using System.Web;
//========
using System.ComponentModel.DataAnnotations;

namespace MVC442.Models.ViewModel
{
    public class WebboardQuizVM : BaseVM 
    {
        [Key]
        [Display(Name = "No.")]
        public int QuizId { get; set; }

        [Required(ErrorMessage = "กรุณาระบุ-หัวข้อ")]
        [Display(Name = "หัวข้อ")]
        public string QuizTopic { get; set; }

  
        [Display(Name = "ข้อความ")]
        public string QuizMessage { get; set; }

        [Required(ErrorMessage = "กรุณาระบุ-ชื่อ")]
        [Display(Name = "ชื่อ")]
        public string QuizName { get; set; }

        [Required(ErrorMessage = "กรุณาระบุ-อีเมล์")]
        [Display(Name = "อีเมล")]
        [EmailAddress] 
        public string QuizEmail { get; set; }


       public string QuizAttach1 { get; set; }
       public string QuizAttach2 { get; set; }
       public string QuizAttach3 { get; set; }

        public Nullable<System.DateTime> QuizDate { get; set; } 
        public string QuizIp { get; set; }
      
 


    }

}