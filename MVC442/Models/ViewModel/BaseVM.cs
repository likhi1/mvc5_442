﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace MVC442.Models.ViewModel {
    public class BaseVM {

        //============= Set Global ==============// 

        //\\\\\\  Dropdown Casino Name  
        [Required(ErrorMessage = "กรุณาระบุ-ชื่อคาสิโน")]
        public int IdCasinoCatSelected { get; set; }  // Keep Save Tempolary  Selected dropdown 
        public IEnumerable<SelectListItem> SelectListItems { get; set; }//  Keep Save Tempolary  Get From  Database 


        //\\\\\\    Radiobox by Enum
        public enum Status {
            None,
            [Display(Name = "ไม่อนุมัติ")] // Get DisplayAttribute  >>>   @(AliasEnum.NoApprove.GetDisplayName()) 
            NoApprove = 'N', //  Get Enum-Key  >>>  AliasEnum.NoApprove   // Get Enum-Value  >>>  ((char)AliasEnum.NoApprove).ToString()  
            [Display(Name = "อนุมัติ")]  // Get DisplayAttribute  >>>   @(AliasEnum.Approve.GetDisplayName()) 
            Approve = 'A'   //  Get Enum-Key  >>>  AliasEnum.Approve   // Get Enum-Value  >>>  ((char)AliasEnum.Approve).ToString()
        }

        //\\\\\\    Initail ListCasino by  List<ListCasino>  //\\\\ Reduce response time query data from database    



 
        List<ListCasino> listCasinos = new List<ListCasino> {
                new ListCasino(){ CasinoId = 1, CasinoName = "SBOBET"  , CasinoType=  1} ,
                new ListCasino() { CasinoId = 2 ,CasinoName =  "IBCBET", CasinoType= 1} ,
                new ListCasino() { CasinoId = 3 ,CasinoName =   "SB1888",  CasinoType= 1} ,
                new ListCasino() { CasinoId = 4 , CasinoName = "Holiday-Palace", CasinoType = 2 } ,
                new ListCasino() { CasinoId = 5 , CasinoName = "G-Club", CasinoType = 2 },
                new ListCasino() { CasinoId = 6 , CasinoName = "Red Dragon", CasinoType = 2 } ,
                new ListCasino() {  CasinoId =7 , CasinoName = "855Crown", CasinoType = 2 } ,
                new ListCasino() {  CasinoId =8 , CasinoName = "Royal 1688", CasinoType = 2 } ,
                new ListCasino() { CasinoId = 9, CasinoName = "Genting Crown", CasinoType = 2 } ,
                new ListCasino() {  CasinoId =10 , CasinoName = "Royal Online Entertainment", CasinoType = 2 } ,
                new ListCasino() { CasinoId = 11 , CasinoName = "SBO Casino & Game", CasinoType = 2 }
        };

 

    public class ListCasino   {
            public int CasinoId { get; set; }
            public string CasinoName { get;  set; }
            public int CasinoType { get; set; }

        }

    } // end  class BaseVM  

}