﻿using System;
using System.Collections.Generic; 
using System.Linq;
using System.Web;
//============
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using MVC442.Models;
using MVC442.Helper.UT;  // referance method in  folder Helper
//==================
using System.Globalization;


namespace MVC442.Models.ViewModel {
    public class SlideManageVM : BaseVM {
 
        public SlideManageVM () {
          
        }

        #region  VieModel
        [Display(Name = "No.")]
        public int SlideId { get; set; }
 
        [Required(ErrorMessage = "กรุณาระบุ-SlideName")]
        public string SlideName { get; set; } 
         
        [Required(ErrorMessage = "กรุณาระบุ-SlideLink")]
        [DataType(DataType.MultilineText)]
        public string SlideLink { get; set; }

        [Required(ErrorMessage = "กรุณาระบุ-SlideDes")]
        [DataType(DataType.MultilineText)]
        public string SlideDes { get; set; }

 
        public Nullable<int> SlideStatus { get; set; }

     
        public Nullable<int> SlideSort { get; set; }


     [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy HH:mm}"  , ApplyFormatInEditMode = true)]  // Set  Global  DateTime Format         
        public DateTime?   SlideInputDate { get; set; }

    [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy HH:mm}", ApplyFormatInEditMode = true)]  // Set  Global  DateTime Format           
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please select a SlideDateShow ")]
        public DateTime? SlideDateShow { get; set; }

     [DisplayFormat(DataFormatString = "{0:HH:mm}", ApplyFormatInEditMode = true)] // Set  Global  DateTime Format    
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please select a SlideDateHide")]
        public DateTime?  SlideDateHide { get; set; }

        #endregion  VieModel


        #region  Set DisplayData 

        ///////////////////////////  Set Display Data  For @Html.DisplayFor( ) /////////////////////////////////////////////  
        [Display(Name = "ชื่อภาพ")]
        [Required(ErrorMessage = "กรุณาระบุ-SlideName")]
        public string SlideNameDisplay {  //// Set Display - SlideName  Short Name  
            get {
                var slideNameDisplay = (!string.IsNullOrEmpty(SlideName)) ? SlideName.Replace("/Upload/images/Slide/", "") : null;
                return slideNameDisplay;
            }

        }

        [Display(Name = "สถานะ")]
        public string  SlideStatusDisplay { 
        get { 
                var slideStatusDisplay = (SlideStatus == 1 )?  "check-right.png"  :  "check-wrong.png"  ; 
                string strImage =  "< img src = '/Content/images/backend/"+ slideStatusDisplay +"' /> "; 
                return slideStatusDisplay;
            } 
        }

        [Display(Name = "ลำดับ")]
        public Nullable<int> SlideSortDisplay { // NullAble no effect  ( Cause  we set default  data  9999 )
            get {
                var slideSortDisplay = (SlideSort == 9999) ? null : SlideSort ;
                return slideSortDisplay;
            }
            //set {
            //    var slideSortDisplay =  value;   
            //  //  var slideSortDisplay = (value == null) ? 9999 : value;  //  Set Default Value  if  Null to  Database 
            //}
        }

      
        [Display(Name = "วันสร้าง")]
        public string  SlideInputDateDisplay { //// Set Display - SlideInputDate  Local Timzone
            get {
                string slideInputDateDisplay = null;
                if (SlideInputDate != null) {
                    DateTime? slideInputDate = Utillity.SetLocalTimeZone(SlideInputDate);  // Get TimeZone befor Convert to StringFormat

                    //\\\\\\\\ Convert DateTime to  StringFormat 
                    DateTime dateTimeUs = (DateTime)slideInputDate; // convert   DateTime?  to  DateTime  // From default US-Format to TH-Format (If no set web.config)
                    slideInputDateDisplay = dateTimeUs.ToString("dd MMM yy  HH:mm", new CultureInfo("th-TH"));
                }
                return slideInputDateDisplay; 
            }
            //set {
            //    var  slideInputDateDisplay = value;
            //}
           
        }

        [Display(Name = "วันเริ่มแสดง")] 
        [Required(AllowEmptyStrings = false, ErrorMessage = "Please select a SlideDateShow ")]
        public string SlideDateShowDisplay { //// Set Display - SlideDateShow  Local Timzone
            get {
                string slideDateShowDisplay = null;
                if ( SlideDateShow != null) { 
                var slideDateShow =  Utillity.SetLocalTimeZone(SlideDateShow); // Get TimeZone befor Convert to StringFormat 
                //\\\\\\\\ Convert DateTime to  StringFormat 
                DateTime dateTimeUs = (DateTime)slideDateShow; // convert   DateTime?  to  DateTime  // From default US-Format to TH-Format (If no set web.config)
                 slideDateShowDisplay = dateTimeUs.ToString("dd MMM yy  HH:mm", new CultureInfo("th-TH"));
                }
                return slideDateShowDisplay;
            }

            //set {
            //   var  slideDateShowDisplay = value ; 
            //}
        }

        [Display(Name = "วันหยุดแสดง")] 
        public string SlideDateHideDisplay { //// Set Display - SlideDateHide  Local Timzone
            get {
                string slideDateHideDisplay = null;
                if (SlideDateHide != null) {
                    var slideDateHide = Utillity.SetLocalTimeZone(SlideDateHide); // Get TimeZone befor Convert to StringFormat

                    //\\\\\\\\ Convert DateTime to  StringFormat 
                    DateTime dateTimeUs = (DateTime)slideDateHide; // convert   DateTime?  to  DateTime  // From default US-Format to TH-Format (If no set web.config)
                    slideDateHideDisplay = dateTimeUs.ToString("dd MMM yy  HH:mm", new CultureInfo("th-TH"));
                }
                return slideDateHideDisplay;
            } 
            //set {
            //   var   slideDateHideDisplay = value;
            //    // var slideDateHideDisplay = (SlideDateHide == null) ? new DateTime().AddYears(1)  : value;    //\\  Set Default Value  if  Null to  Database 
            //}

        }


       

        #endregion  DisplayData 

    }
}