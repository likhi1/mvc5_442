﻿using MVC442.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MVC442.Helper.UT;  // referance method in  folder Helper

namespace MVC442.Models.ViewModel {
    public class WebLinkLiveScoreVM : BaseVM {

        //[HiddenInput(DisplayValue = false)]  
        [Display(Name = "No.")]
        public int IdCon { get; set; }
         

        [Display(Name = "ประเภท")]
        public Nullable<int> ConType { get; set; }

        [Required(ErrorMessage = "กรุณาระบุ-ชื่อลิงค์")] 
        [Display(Name = "ชื่อเวบ")]
        public string WebName { get; set; }

        [Required(ErrorMessage = "กรุณาระบุ-ลิงค์URL")]
        [DataType(DataType.MultilineText)]
        [Display(Name = "ลิงค์เวบ")]
        public string WebLink { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d.MMM.yyyy  HH:mm }")]
        [DataType(DataType.Date)]
        [Display(Name = "วัน-เวลา")] 
        public Nullable<System.DateTime> InputDate { get; set; }

        public virtual CasinoCat CasinoCat { get; set; }


        ///////////////////////////  Create Custom Display Value /////////////////////////////////////////////
       
        ///// This for set Trim of Message
        [Display(Name = "ลิงค์เวบ")]
        public string WebLinkTrimmed {
            get {
                if ((WebLink.Length) > 20) {
                    return WebLink.Substring(0, 20) + "...";
                }
                else {
                    return WebLink;
                }
            }
        }


        DateTime? inputDateDisplay;
        public DateTime? InputDateDisplay {

            get {
                inputDateDisplay = Utillity.SetLocalTimeZone(InputDate);
                return inputDateDisplay;
            }
            set {
                inputDateDisplay = value;
            }
        }


      


    }
}