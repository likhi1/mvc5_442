﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//============
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;
using MVC442.Models;
using MVC442.Helper.UT;  // referance method in  folder Helper

namespace MVC442.Models.ViewModel {

    public class ClientRegisterVM : BaseVM {

        [Key]
        [Display(Name = "No.")]
        public int IdClientRegister { get; set; }

        [Required(ErrorMessage = "กรุณาระบุ-ประเภทคาสิโน")]
        [Display(Name = "ประเภทบริการ")]
        public  int  IdCasinoCat { get; set; }

        [Required(ErrorMessage = "กรุณาระบุ-ชื่อ")]
        [Display(Name = "ชื่อ")]
        public string Name { get; set; }

        [Required(ErrorMessage = "กรุณาระบุ-อีเมล์")]
        [EmailAddress(ErrorMessage = "กรุณาระบุ-อีเมล์ ให้ถูกต้อง")] 
        [Display(Name = "อีเมล์")] 
        public string Email { get; set; }

        [Required(ErrorMessage = "กรุณาระบุ-เบอร์โทร")]
        [Display(Name = "เบอร์โทร")]
        public string Tel1 { get; set; }

        [Display(Name = "รับSMS")]
        public bool GetSms { get; set; }

        [Display(Name = "รับข่าว")]
        public bool GetNews { get; set; }

        //[Required(ErrorMessage = "Enter the Issued date.")] 
        [Display(Name = "วัน-เวลา")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d.MMM.yyyy  HH:mm }")] 
        [DataType(DataType.Date)]
        public DateTime? InputDate { get; set; }   //  Null Able 

        public string Ip { get; set; }

        public string CatNameThShow { get; set; } // Create this  cause we want  send value by ViewModel

        //\\\\\\\\\\\\\\\\\\\  Many to One Relation ( For referrance Casino Csategory )  
        public virtual CasinoCat CasinoCat { get; set; }    


        //\\\\\\\\\\\\\\\\\\ Create Custom Display Value  
        DateTime? inputDateDisplay;
        public DateTime? InputDateDisplay {

            get {
                inputDateDisplay = Utillity.SetLocalTimeZone(InputDate);
                return inputDateDisplay;
            }
            set {
                inputDateDisplay = value; 
            }
        }
    


        //================ Test  send model direct to view  DropDown ==========================//
        ////////// Test Bind 1
        //[Required(ErrorMessage = "xxxxxxxxxxxxx")]
        //public virtual IEnumerable<SelectListItem> CasinoCat { get; set; }
         
        //private mvc442Entities db = new mvc442Entities(); // connect Entity

        //public ClientRegisterVM(int idClientRegister) {
        //    IdClientRegister = idClientRegister;  // set value from controler
        //    CasinoCat = GetSelectList();
        //}

        //private IEnumerable<SelectListItem> GetSelectList() {  // 
        //    IEnumerable<SelectListItem> selectList = db.CasinoCats.OrderBy(x => x.IdCasinoCat).ToList()  // Use LINQ to Entity  cause can use .ToString()
        //                                            .Select(x => new SelectListItem {
        //                                                Selected = (x.IdCasinoCat == IdClientRegister),
        //                                                Value = x.IdCasinoCat.ToString(),
        //                                                Text = x.CatNameTh.ToString()
        //                                            });  
        //    return selectList;
        //}
         
        ////////// Test Bind 2
        //public   ClientRegisterVM() {
        //    List<SelectListItem> CasinoCat = db.CasinoCats.Select(x => new SelectListItem { Value = x.IdCasinoCat.ToString(), Text = x.CatNameTh }).ToList();

        //}

        ////////// Test Bind 3
        //public List<SelectListItem> CSCat {
        //    get {
        //        List<SelectListItem> list = (from c in db.CasinoCats select new SelectListItem { Value = c.IdCasinoCat.ToString(), Text = c.CatNameTh }).ToList();
        //        list.Insert(0, new SelectListItem { Value = "0", Text = "[Select an object]" });
        //        return list;
        //    }
        //}


    }
}

