﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using System;

namespace MVC442.Models {

    //Likhit  12/17/2015   // Increse For PK nvarchar  to PK Int
    public class UserRoleIntPk : IdentityUserRole<int> {
    }

    public class UserClaimIntPk : IdentityUserClaim<int> {
    }

    public class UserLoginIntPk : IdentityUserLogin<int> {
    }
     
    public class RoleIntPk : IdentityRole<int, UserRoleIntPk> {
        public RoleIntPk() { }
        public RoleIntPk(string name) { Name = name; }
    }

    public class UserStoreIntPk : UserStore<ApplicationUser, RoleIntPk, int,
        UserLoginIntPk, UserRoleIntPk, UserClaimIntPk> {
        public UserStoreIntPk(ApplicationDbContext context)
            : base(context) {
        }
    }

    public class RoleStoreIntPk : RoleStore<RoleIntPk, int, UserRoleIntPk> {
        public RoleStoreIntPk(ApplicationDbContext context)
            : base(context) {
        }
    }


    //Likhit  12/17/2015   //  Increse For Role Manage 
    public class ApplicationUserLogin : IdentityUserLogin<int> { }
    public class ApplicationUserClaim : IdentityUserClaim<int> { }
    public class ApplicationUserRole : IdentityUserRole<int> { }

   
    //Likhit  12/17/2015   // Edit  PK nvarchar  to PK Int   by replace <ApplicationUser> with <ApplicationUser, int>
    public class ApplicationUser : IdentityUser <int, UserLoginIntPk, UserRoleIntPk, UserClaimIntPk> {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser , int> manager) {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        //Likhit  13/11/2015  Customize Model  AspNet.Identity  ( On Table  AspNetUsers )
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string NameDisplay { get; set; }
        //public string BirthDate { get; set; }

    }
     
    public class ApplicationRole : IdentityRole<int, ApplicationUserRole>, IRole<int> {
        public string Description { get; set; }

        public ApplicationRole() : base() { }
        public ApplicationRole(string name) : this() {
            this.Name = name;
        }

        public ApplicationRole(string name, string description) : this(name) {
            this.Description = description;
        }
    }

    public class ApplicationRoleStore
 : RoleStore<ApplicationRole, int, ApplicationUserRole>,
 IQueryableRoleStore<ApplicationRole, int>,
 IRoleStore<ApplicationRole, int>, IDisposable {
        public ApplicationRoleStore()
            : base(new IdentityDbContext()) {
            base.DisposeContext = true;
        }

        public ApplicationRoleStore(DbContext context)
            : base(context) {
        }
    }
      
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser, RoleIntPk, int,
        UserLoginIntPk, UserRoleIntPk, UserClaimIntPk> {
        public ApplicationDbContext()
            : base("IdentityDbContext" ) {
        }

        public static ApplicationDbContext Create() {
            return new ApplicationDbContext();
        }
    }




}  // end namespace


