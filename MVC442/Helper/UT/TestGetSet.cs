﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC442.Helper.UT {
    // This class is mutable. Its data can be modified from 
    // outside the class. 
    class Customer {
        // Auto-Impl Properties for trivial get and set 
        public double TotalPurchases { get; set; }
        public string Name { get; set; }
        public int CustomerID { get; set; }

        public Customer() {

        }
        // Constructor 
        public Customer(double purchases, string name, int ID) {
            TotalPurchases = purchases;
            Name = name;
            CustomerID = ID;
        }
        
    }

    
       
}