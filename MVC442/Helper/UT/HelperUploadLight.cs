﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
//========
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;


namespace MVC442.Helper.UT {
    public class HelperUploadLight {
         
        static readonly string ItemUploadFolderPath = "~/Upload/webboard";
        public string finalFileName { get; private set;  }   // New file name 
        public bool sucess { get; private set; }  // Success or not

        //========== Constructer
        public  HelperUploadLight( ) { 

        }
 
        //============ Only Single Upload 
        public void  SendFileUpload(HttpPostedFileBase file) {

            if (file.ContentLength > 0) {
                ///// Get name & extention of fileUpload
                var fileName = Path.GetFileName(file.FileName);
                string extension = Path.GetExtension(fileName);

                //////// Rename by  Random String
                var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
                var random = new Random();
                var newFileName = new string(
                Enumerable.Repeat(chars, 8)
                .Select(s => s[random.Next(s.Length)])
                .ToArray());

                ///// New file Name of fileUpload
                finalFileName = newFileName + extension;

                ////// Get path to upload
                var path = Path.Combine(HttpContext.Current.Request.MapPath(ItemUploadFolderPath), finalFileName);

                ////// Check exists  file  by path 
                if (!File.Exists(path)) {
                    ////// UploadFile to servers
                    file.SaveAs(path);  
                    //////// resize photo 
                    Image imgOriginal = Image.FromFile(path);
                    Image imgActual = ScaleBySize(imgOriginal, 600);
                    imgOriginal.Dispose();
                    imgActual.Save(path);
                    imgActual.Dispose();

                    Debug.WriteLine("success upload");
                    sucess = true;
                } else {
                    Debug.WriteLine("Can't  upload");
                    sucess = false;
                }

            }
             
        } // end method

         

        public static Image ScaleBySize(Image imgPhoto, int size) {
            int logoSize = size;

            float sourceWidth = imgPhoto.Width;
            float sourceHeight = imgPhoto.Height;
            float destHeight = 0;
            float destWidth = 0;
            int sourceX = 0;
            int sourceY = 0;
            int destX = 0;
            int destY = 0;

            // Resize Image to have the height = logoSize/2 or width = logoSize.
            // Height is greater than width, set Height = logoSize and resize width accordingly
            if (sourceWidth > (2 * sourceHeight)) {
                destWidth = logoSize;
                destHeight = (float)(sourceHeight * logoSize / sourceWidth);
            } else {
                int h = logoSize / 2;
                destHeight = h;
                destWidth = (float)(sourceWidth * h / sourceHeight);
            }
            // Width is greater than height, set Width = logoSize and resize height accordingly

            Bitmap bmPhoto = new Bitmap((int)destWidth, (int)destHeight,
                                        PixelFormat.Format32bppPArgb);
            bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);

            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;

            grPhoto.DrawImage(imgPhoto,
                new Rectangle(destX, destY, (int)destWidth, (int)destHeight),
                new Rectangle(sourceX, sourceY, (int)sourceWidth, (int)sourceHeight),
                GraphicsUnit.Pixel);

            grPhoto.Dispose();

            return bmPhoto;
        }


         
         
    } // end class
}