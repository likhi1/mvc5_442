﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MVC442.Helper.UT {
    public class HelperUtility {

        //======== Use Automatic Properties
        public string Message { get; private set; }  // Message for return sucess or fail   & private field  on set 
        public bool Feedback { get;  set; }  //  

        //======= Use Constructer
        public HelperUtility(bool fb  ) {
            Feedback  = fb;
            ResponseMessage(fb);
        }

        public string ResponseMessage(bool Feed) {
            if (Feed == true) { 
                Message = "This Insert Success";
            } else {
                Message = "This Not Insert";
            } 
            return Message ;
        }


 

    }// end class  
} // end namespace